#!/bin/bash

for VERSIE in 1.0 1.1 1.2 2.0 2.1 3.0; do
  docker build -f Dockerfile --build-arg VERSIE=$VERSIE -t registry.gitlab.com/commonground/haven/hallo-wereld:$VERSIE .
  docker push registry.gitlab.com/commonground/haven/hallo-wereld:$VERSIE
  sleep 60
done
